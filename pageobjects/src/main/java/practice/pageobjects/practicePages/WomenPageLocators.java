package practice.pageobjects.practicePages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WomenPageLocators {

    @FindBy(xpath = "//div[@class='rte']/p/strong[text()= 'You will find here all woman fashion collections.' ]")
    public WebElement bannerText_WomenFashion_text;

    @FindBy(xpath = "//div[@class='rte']/p/following-sibling::p[text()= 'This category includes all the basics of your wardrobe and much more:']")
    public WebElement bannerText_WomenFashion_text2;

    @FindBy(xpath = "//div[@class='rte']/p/following-sibling::p[2]")
    public WebElement bannerText_WomenFashion_text3;
}

