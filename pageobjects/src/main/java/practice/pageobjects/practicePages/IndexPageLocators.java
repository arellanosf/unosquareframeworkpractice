package practice.pageobjects.practicePages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class IndexPageLocators {

    @FindBy(xpath="//a[@title='Women' and text()='Women']")
    public WebElement navigationOption_women_a;
}
