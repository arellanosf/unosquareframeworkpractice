package practice.pageobjects.practicePages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPageLocators {

    @FindBy(xpath = "//input[@id='email_create']")
    public WebElement createAnAccount_emailAddress_inputText;

    @FindBy(xpath = "//input[@id='SubmitCreate']")
    public WebElement createAnAccount_submit_inputButton;

    @FindBy(xpath = "//form[@id='create-account_form']/h3[text()= 'Create an account']")
    public WebElement createAnAccount_text_header3;
}
