package practice.pageobjects.practicePages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HeaderPageLocators {

    @FindBy(xpath="//a[@title='Log in to your customer account']")
    public WebElement headerOption_signIn_a;

}
