package practice.pageobjects.practicePages;

import org.openqa.selenium.WebDriver;
import practice.framework.core.BasePage;

public class WomenPage extends BasePage {

    private WomenPageLocators womenPageLocators = (WomenPageLocators) super.InitializedPage;

    public Boolean flag = false;

    public WomenPage(WebDriver driver) {
        super(driver, WomenPageLocators.class);
    }

    public Boolean assertTextFromWomenBanner() {
        flag = elementIsVisible(womenPageLocators.bannerText_WomenFashion_text);
        return flag;
    }
    public Boolean assertTextFromWomenBannerSecondSection() {
        flag = elementIsVisible(womenPageLocators.bannerText_WomenFashion_text2);
        return flag;
    }
    public Boolean assertTextFromWomenBannerThirdSection() {
        flag = elementIsVisible(womenPageLocators.bannerText_WomenFashion_text3);
        return flag;
    }

}
