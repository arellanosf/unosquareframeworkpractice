package practice.pageobjects.practicePages;

import org.openqa.selenium.WebDriver;
import practice.framework.core.BasePage;

public class HeaderPage extends BasePage {

    private HeaderPageLocators headerPageLocators = (HeaderPageLocators) super.InitializedPage;

    public HeaderPage(WebDriver driver) {
        super(driver, HeaderPageLocators.class);
    }

    public void doClickHeaderOption_SignInInputButton() {
        doClick(headerPageLocators.headerOption_signIn_a);
    }
}