package practice.pageobjects.practicePages;

import org.openqa.selenium.WebDriver;
import practice.framework.core.BasePage;

public class SignInPage extends BasePage {

    public Boolean flag = false;

    private SignInPageLocators signInPageLocators = (SignInPageLocators) super.InitializedPage;

    public SignInPage(WebDriver driver) {
        super(driver, SignInPageLocators.class);
    }

    public void insertEmailAddress() {
        enterTextInput(signInPageLocators.createAnAccount_emailAddress_inputText, "arellanosf@gmail.com");
    }

    public void doClickCreateAnAccount_SubmitButton() {
        doClick(signInPageLocators.createAnAccount_submit_inputButton);
    }

    public Boolean assertTextFromSignInPageCreateAnAccountForm() {
        flag = elementIsVisible(signInPageLocators.createAnAccount_text_header3);
        return flag;
    }

}
