package practice.tests.practiceTest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import practice.framework.core.BaseTest;
import practice.pageobjects.practicePages.HeaderPage;
import practice.pageobjects.practicePages.IndexPage;
import practice.pageobjects.practicePages.SignInPage;
import practice.pageobjects.practicePages.WomenPage;

public class PracticeTest extends BaseTest {

    private WebDriver driver = getDriver();

    @Test
    public void practiceTest() throws InterruptedException {
        Thread.sleep(3000);

        IndexPage indexPage = new IndexPage(driver);
        indexPage.doClickNavigationOpton_OnWomenLink();

        WomenPage womenPage = new WomenPage(driver);
        Assert.assertTrue(womenPage.assertTextFromWomenBanner());
        Assert.assertTrue(womenPage.assertTextFromWomenBannerSecondSection());
        Assert.assertTrue(womenPage.assertTextFromWomenBannerThirdSection());

        HeaderPage headerPage = new HeaderPage(driver);
        headerPage.doClickHeaderOption_SignInInputButton();

        SignInPage signInPage = new SignInPage(driver);
        Assert.assertTrue(signInPage.assertTextFromSignInPageCreateAnAccountForm());
        signInPage.insertEmailAddress();
        signInPage.doClickCreateAnAccount_SubmitButton();

        Thread.sleep(3000);
    }

}
